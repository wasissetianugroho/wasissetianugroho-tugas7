import {
    Alert,
    FlatList,
    Image,
    Modal,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
  } from 'react-native';
  import React, {useEffect, useState} from 'react';
  import {BASE_URL, TOKEN} from './Url';
  import Icon from 'react-native-vector-icons/AntDesign';
  import {useIsFocused} from '@react-navigation/native';
  import AddData from './AddData';
  
  const Home = ({navigation, route}) => {
    const [modalVisible, setModalVisible] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const isFocused = useIsFocused();

    const [dataMobil, setDataMobil] = useState([]);
    const [dataModal, setDataModal] = useState(null);

    useEffect(() => {
      getDataMobil();
      setIsLoading(true);
      console.log('is focused: ', isFocused);
    }, [isFocused]);
  
    const getDataMobil = async () => {
      try {
        const response = await fetch(`${BASE_URL}mobil`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            Authorization: TOKEN,
          },
        });
  
        const result = await response.json();
        console.log('Success:', result.items[0].title);
        setDataMobil(result.items);
        setIsLoading(false);
      } catch (error) {
        console.error('Error:', error);
      }
    };
  
    const showLoading = () => {
      return (
        <Modal animationType="fade" transparent={true} visible={isLoading}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                padding: 16,
                backgroundColor: 'white',
                borderRadius: 20,
                elevation: 4,
              }}>
              <Image
                source={require('./image/Loading.gif')}
                style={{width: 250, height: 250}}
              />
            </View>
          </View>
        </Modal>
      );
    };
  
    const convertCurrency = (nominal = 0, currency) => {
      let rupiah = '';
      const nominalref = nominal.toString().split('').reverse().join('');
      for (let i = 0; i < nominalref.length; i++) {
        if (i % 3 === 0) {
          rupiah += nominalref.substr(i, 3) + '.';
        }
      }
  
      if (currency) {
        return (
          currency +
          rupiah
            .split('', rupiah.length - 1)
            .reverse()
            .join('')
        );
      } else {
        return rupiah
          .split('', rupiah.length - 1)
          .reverse()
          .join('');
      }
    };  
  
    // const [dataMobil, setDataMobil] = useState([]);
    // useEffect(() => {
    //   getDataMobil();
    //   console.log('is focused: ', isFocused);
    // }, [isFocused]);
  
    // const getDataMobil = async () => {
    //   try {
    //     const response = await fetch(`${BASE_URL}mobil`, {
    //       method: 'GET',
    //       headers: {
    //         'Content-Type': 'application/json',
    //         Authorization: TOKEN,
    //       },
    //     });
  
    //     const result = await response.json();
    //     console.log('Success:', result.items[0].title); // Success: Tayo
    //     setDataMobil(result.items);
    //   } catch (error) {
    //     console.error('Error:', error);
    //   }
    // };
  
    // const convertCurrency = (nominal = 0, currency) => {
    //   let rupiah = '';
    //   const nominalref = nominal.toString().split('').reverse().join('');
    //   for (let i = 0; i < nominalref.length; i++) {
    //     if (i % 3 === 0) {
    //       rupiah += nominalref.substr(i, 3) + '.';
    //     }
    //   }
  
    //   if (currency) {
    //     return (
    //       currency +
    //       rupiah
    //         .split('', rupiah.length - 1)
    //         .reverse()
    //         .join('')
    //     );
    //   } else {
    //     return rupiah
    //       .split('', rupiah.length - 1)
    //       .reverse()
    //       .join('');
    //   }
    // };
  
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        {/* MODAL POP UP ============================================================ */}
        {showLoading()}
      <AddData
        modalVisible={modalVisible}
        onCloseModal={() => {
          setModalVisible(false);
          getDataMobil();
        }}
        dataModal={dataModal}
        setDataModal={setDataModal}
      />
        {/* <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
            setModalVisible(!modalVisible);
          }}>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <TouchableOpacity
              onPress={() => setModalVisible(false)}
              style={{
                height: '100%',
                width: '100%',
                position: 'absolute',
              }}></TouchableOpacity> */}
            {/* <View
              style={{
                backgroundColor: 'red',
                margin: 24,
              }}>
              <Text>Hello Guys!</Text>
              <Pressable onPress={() => setModalVisible(!modalVisible)}>
                <Text>Hide Modal</Text>
              </Pressable>
            </View> */}
          {/* </View> */}
        {/* </Modal> */}
  
        {/* LIST ITEM =================================================================================== */}
        <Text
          style={{fontWeight: 'bold', fontSize: 20, margin: 15, color: '#000'}}>
          Home screen
        </Text>
        <FlatList
          data={dataMobil}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => (
            <TouchableOpacity
              onPress={() => {
                // navigation.navigate('AddData', item)
                setModalVisible(true);
                setDataModal(item);
              }}
              activeOpacity={0.8}
              style={{
                width: '90%',
                alignSelf: 'center',
                marginTop: 15,
                borderColor: '#dedede',
                borderWidth: 1,
                borderRadius: 6,
                padding: 12,
                flexDirection: 'row',
              }}>
              <View
                style={{
                  width: '30%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  style={{width: '90%', height: 100, resizeMode: 'contain'}}
                  source={{uri: item.unitImage}}
                />
              </View>
              <View
                style={{
                  width: '70%',
                  paddingHorizontal: 10,
                }}>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                    Nama Mobil :
                  </Text>
                  <Text style={{fontSize: 14, color: '#000'}}> {item.title}</Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                    Total KM :
                  </Text>
                  <Text style={{fontSize: 14, color: '#000'}}>
                    {' '}
                    {item.totalKM}
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                    Harga Mobil :
                  </Text>
                  <Text style={{fontSize: 14, color: '#000'}}>
                    {'Rp '}
                    {convertCurrency(item.harga)}
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                    Tahun :
                  </Text>
                  <Text style={{fontSize: 14, color: '#000'}}>
                    {' '}
                    {item.tahun}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
  
        {/* PLUS BUTTON ======================================================================== */}
        <TouchableOpacity
          style={{
            position: 'absolute',
            bottom: 25,
            right: 25,
            width: 45,
            height: 45,
            borderRadius: 15,
            backgroundColor: 'grey',
          }}
          onPress={() => {
            setModalVisible(true);
            setDataModal(null);
          }}>
          <Icon name="plus" size={45} color="white" />
        </TouchableOpacity>
      </View>
    );
  };
  
  const styles = StyleSheet.create({
    safeAreaContent: {
      flex: 1,
      backgroundColor: 'red',
    },
  });
  
  export default Home;  