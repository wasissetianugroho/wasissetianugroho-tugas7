import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {NavigationContainer} from '@react-navigation/native';
import Home from './Home';
import AddData from './AddData';

const Stack = createNativeStackNavigator();

const Routing = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{headerShown: false}}
        initialRouteName="HomeScreen">
        <Stack.Screen name="HomeScreen" component={Home} />
        {/* <Stack.Screen name="AddData" component={AddData} /> */}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routing;

